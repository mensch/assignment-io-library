section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.cycle:
		cmp byte[rdi+rax], 0x0
		je .end
		inc rax
		jne .cycle
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	add rsp, 8
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    mov r8, rsp
    dec rsp
    xor al, al
    mov [rsp], al
    mov rax, rdi
    mov rcx, 10

	.loop:
		xor rdx, rdx ;тут будет остаток
		div rcx ;делим на 10
		add rdx, '0' ;перевод в ascii
		dec rsp ;смещаем адрес стека
		mov [rsp], dl ;записываем символ в стек
		cmp rax, 0 
		jne .loop
		mov rdi, rsp
		call print_string
		mov rsp, r8
		ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .neg_num
    jmp .print_num

	.neg_num:
		neg rdi
		mov r8, rdi
		mov rdi, '-'
		call print_char
		mov rdi, r8

	.print_num:
		jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rcx,0

	.strcmp_loop:
		mov byte dl,[rdi+rcx]
		mov byte dh,[rsi+rcx]
		inc ecx
		cmp dl,0
		je .strcmp_equal
		cmp byte dl,dh
		je .strcmp_loop
		jne .strcmp_not_equal

	.strcmp_equal:
		cmp dh,0
		jne .strcmp_not_equal
		mov rax, 1
		ret
		
	.strcmp_not_equal:
		mov rax, 0
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    dec rsp
	mov byte[rsp], 0x0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
	mov al, byte[rsp]
	inc rsp
    ret 

; Принимает: адрес начала буфера rdi, размер буфера rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	mov r9, rsp
	xor r8, r8
	xor rdx, rdx
	xor rcx, rcx
	
	.read_char:
		push rdi
		push rsi
		push r8
		push rdx
		call read_char
		pop rdx
		pop r8
		pop rsi
		pop rdi
		cmp rdx, 0
		jne .check_tabulation_inside
		
	.check_tabulation:
		cmp al, 0x20
        je .read_char
        cmp al, 0x9
        je .read_char
        cmp al, 0xA
        je .read_char
		cmp al, 0x0
        je .empty
		
	.check_tabulation_inside:
		cmp al, 0x20
        je .empty_accumulator
		cmp al, 0x9
        je .empty_accumulator
		cmp al, 0xA
        jne .save_byte_to_stack
		
	.empty_accumulator:
		xor rax, rax
		
	.save_byte_to_stack:
		dec rsp
		inc rdx
		cmp rdx, rsi
		jg .not_enough_space
		mov byte[rsp], al
		
	.check_if_zero_or_space:	
		cmp al, 0x0
        jne .read_char
	
	.presave:
		mov rcx, rdx
		
	.save_byte_to_buffer:
		mov r8b, byte[rsp]
		mov byte[rdi+rcx-1], r8b
		dec rcx
		inc rsp
		cmp rcx, 0
		jne .save_byte_to_buffer
		mov rax, rdi
		dec rdx
		ret
		
	.not_enough_space:
		xor rax, rax
		xor rdx, rdx
		mov rsp, r9
		ret
		
	.empty:
		mov rdx, 1
		cmp rdx, rsi
		jg .not_enough_space
		mov byte[rdi], 0x0
		mov rax, rdi
		mov rsp, r9
		xor rdx, rdx
		ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rdx, rdx
	xor rcx, rcx
    xor r8, r8
	mov r9, 10

    .parse_char:
        mov r8b, byte[rdi + rcx]
        cmp r8b, '0'
        jl .return
        cmp r8b, '9'
        jg .return
        inc rcx
        sub r8b, '0'
        mul r9
        add rax, r8
        jmp .parse_char
		
    .return:
        mov rdx, rcx
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    je .parse_negative
	
    .parse_positive:
		jmp parse_uint
	
    .parse_negative:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .finish_parsing
        neg rax
        inc rdx
		
    .finish_parsing:
        ret

; Принимает указатель на строку rdi, указатель на буфер rsi и длину буфера rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rcx, rcx
	xor r9, r9
	call string_length
	inc rax
	cmp rax, rdx
	jg .small_buffer
	
	.cycle:
		cmp byte[rdi+rcx], 0
		je .end
		mov r9, [rdi+rcx]
		mov [rsi+rcx], r9
		inc rcx
		jne .cycle
	
	.small_buffer:
		xor rax, rax
		
	.end:
		cmp rcx, 0
		je .copy_zero
		inc rcx
		
	.copy_zero:
		mov r9, [rdi+rcx]
		mov [rsi+rcx], r9
		ret 